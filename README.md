## Modul Pelatihan Pemrograman Mobile Menggunakan Flutter

Modul ini dibuat dengan menggunakan Quarto, aplikasi untuk membuat buku dan publikasi lain yang ramah dengan penulisan source code pemrograman dan dapat digenerate ke dalam beberapa macam format meliputi web, pdf, word, dan slide.

Untuk dapat menulis dan menambahkan konten pada buku ini dilakukan dengan cara:

1. Install terlebih dahulu Quarto CLI yang dapat di-download [di link resminya](https://quarto.org/docs/get-started/).
2. Ada 3 jenis editor yang dapat digunakan yaitu VS-Code, R-Studio, dan Neovim.
   - Jika menggunakan **VS-Code** perlu install dulu plugin **Quarto** di marketplace
   - **R-Studio** menyediakan visual editor terintegrasi dan plugin yang dapat diinstal dengan mudah
3. Untuk melihat hasilnya jalankan perintah `quarto preview` di terminal.
4. Untuk generate hasilnya menjadi pdf, jalankan perintah `quarto render --to pdf`

### Metadata & Struktur Buku

Metadata dan struktur buku ada di file `_quarto.yml`. Jika perlu menambahkan file *.qmd (quarto markdown) baru, jangan lupa menambahkan dalam daftar chapter di file tersebut.

Semua aset gambar disimpan pada direktori `assets`.

Semua source code sample disimpan pada direktori `sample_source_code` sesuai dengan babnya.