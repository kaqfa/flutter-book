## Todo List dengan SQLite (Tanpa Login)

Tutorial bisa dibagi menjadi ke dalam 2 bagian:

- Kita mengerjakan tampilan dengan form berupa popup dan data todo hanya tersimpan dalam variabel lokal saja.
- Memisahkan form ke dalam page baru, menambahkan database, dan menambahkan search.

### Langkah Pengerjaan Fase 1: 

- [ ] Create Project
- [ ] Pisahkan Material App pada widget baru, dan buat File baru `todo_page.dart` untuk memanggilnya
- [ ] Tambahkan title dan theme pada MaterialApp jika diperlukan.
- [ ] Tambahkan Appbar dengan title `Aplikasi Todo List`
- [ ] Extract Scaffold menjadi widget `TodoList`.
- [ ] Ubah jadi StatefulWidget
	- Implement method `createState` dengan arrow function
	- Buat class extend `State`
	- Pindahkan method `build` ke class `State`
- [ ] Buat file baru untuk `class Todo`
	- Buat dummy data utnuk `class Todo`
- [ ] Kembali ke `TodoPage`, wrap `Center` dengan `Column`
- [ ] Tambahkan data `todoList` dengan inisialisasi `dummyData`
- [ ] Tambahkan `Expanded` > `Listview.builder`.
- [ ] Tambahkan `ListTile` dengan leading `IconButton` check_circle, title, subtitle: description, dan trailing: `IconButton` delete.
- [ ] Tambahkan ternary condition pada leading untuk menggunakan radio_button_unchecked
- [ ] Buat fungsi `updateItem` untuk mengubah completed ketika diklik
- [ ] Buat juga fungsi `refreshList` untuk mengubah tampilan
- [ ] Buat fungsi `deleteItem` untuk menghapus data dan panggil pada `onPressed`nya icon `trash`
- [ ] Tambahkan floatingActionButton dengan FloatingActionButton icons.add
- [ ] Selanjutnya untuk manipulasi form input, Tambahkan 2 atribut `TextEditingController` pada class State
- [ ] Buat fungsi `showPopup` untuk menampilkan form tambah todo dalam bentuk popup
- [ ] Isikan popup tersebut dengan `container`, `column` > 2 `TextField`
	- utnuk mengatur lebar pop up full menggunakan `width: MediaQuery.of(context).size.width`
- [ ] Pada bagian actions tambahkan 2 TextButton

### Langkah Pengerjaan Fase 2 (menambahkan DB):

Yang digunakan untuk penyimpanan adalah sqlite, dimana basis data disimpan dalam bentuk file di penyimpanan lokal. Sehingga kita perlu menggunakan beberapa library tambahan

- [ ] Tambahkan dependesi:
	- `path`: untuk memudahkan akses path direktori local storage
	- `path_provider`: untuk memudahkan akses ke direktori khusus pada tiap OS.
	- `sqflite`: untuk membuat basis data SQLite
- [ ] Siapkan class Todo dengan menambahkan method `toMap` dan `fromMap`reS
- [ ] Ubah class `Todo` dengan menambahkan toMap dan 
- [ ] Buat class `DatabaseHelper` pada `database_helper.dart`
- [ ] Buat koneksi database dengan singleton pattern:
	- Buat atribut instance sebagai internal
	- Buat factory instance
	- Buat atribut static `_db`
	- Buat getter db untuk implementasi singleton
- [ ] Buat method `initDb` secara async
- [ ] Buat method `_oncreate` secara async juga
- [ ] Buat method `getAllTodos`
- [ ] Buat method `insertTodo`
- [ ] Buat method `updateTodo`
- [ ] Buat method `deleteTodo`
- [ ] Tambahkan atribut `dbHelper` pada class State Todo
- [ ] Inisialisasikan dbHelper pada `initState`
- [ ] Ubah `refreshList` jadi async dan tambahkan `getAllTodos`
- [ ] Ubah fungsi `addItem` dengan memanggil fungsi `insertTodo`
- [ ] Ubah fungsi `updateItem` dengan menambahkan pemanggilan fungsi `updateTodo`
- [ ] Ubah fungsi `deleteItem` dengan memanggil fungsi `deleteTodo`
- [ ] Tambahkan method `searchTodo` pada DatabaseHelper
- [ ] Tambahkan method `searchItems` pada TodoState
- [ ] Tambahkan Textfield dan search controller di bagian atas tampilan aplikasi.
- [ ] boleh juga berikan InputDecoration dengan border OutlineInputBorder


