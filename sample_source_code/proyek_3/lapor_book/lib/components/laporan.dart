import 'dart:js';

import 'package:flutter/material.dart';
import 'package:lapor_book/pages/detail_lapor_page.dart';

class Laporan {
  final String judul;
  final String tanggal;
  final String pelapor;
  final String kategori;
  final String foto;
  final String keterangan;

  Laporan(
      {required this.judul,
      required this.tanggal,
      required this.pelapor,
      required this.kategori,
      required this.foto,
      this.keterangan = ''});
}

class ItemLaporan extends StatelessWidget {
  const ItemLaporan({super.key, required this.laporan});

  final Laporan laporan;

  void _itemTap(BuildContext context, Laporan lap) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => (DetailLaporanPage(laporan: lap))));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Card(
        child: InkWell(
          onTap: () => _itemTap(context, laporan),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.all(8),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network(
                    laporan.foto,
                    height: 100,
                    width: 100,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      laporan.judul,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        const Icon(Icons.calendar_today),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(laporan.tanggal),
                        const SizedBox(
                          width: 16,
                        ),
                        const Icon(Icons.person),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(laporan.pelapor)
                      ],
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      laporan.kategori,
                      style: const TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
