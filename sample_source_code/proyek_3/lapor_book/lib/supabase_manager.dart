import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SupabaseManager {
  final client = Supabase.instance.client;

  Future<void> signUpUser(context, {String? email, String? password}) async {
    debugPrint("Email:$email password:$password");
    final AuthResponse res =
        await client.auth.signUp(email: email!, password: password!);

    // debugPrint(res.data!.toString());
    final Session? session = res.session;
    final User? user = res.user;

    if (res.user != null) {
      Fluttertoast.showToast(
          msg: "Registrasi berhasil", toastLength: Toast.LENGTH_SHORT);
      Navigator.pushReplacementNamed(context, 'login');
    } else {
      Fluttertoast.showToast(
          msg: "Registrasi gagal",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: Colors.red[200]);
    }
  }

  Future<void> signInUser(context, {String? email, String? password}) async {
    debugPrint("Email:$email password:$password");
    final AuthResponse res = await client.auth
        .signInWithPassword(email: email!, password: password!);

    final Session? session = res.session;
    final User? user = res.user;

    if (res.user != null) {
      Fluttertoast.showToast(
          msg: "Login berhasil", toastLength: Toast.LENGTH_SHORT);
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      Fluttertoast.showToast(
          msg: "Registrasi gagal",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: Colors.red[200]);
    }
  }

  Future<void> logout(context) async {
    await client.auth.signOut();
    Navigator.pushReplacementNamed(context, 'login');
  }
}
