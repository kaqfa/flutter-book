import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:lapor_book/pages/signup_page.dart';
import 'package:lapor_book/pages/login_page.dart';
import 'package:lapor_book/pages/home_page.dart';
import 'package:lapor_book/pages/splash_page.dart';

const String supabaseUrl = 'https://uqvpsaarzktyttnhqzmf.supabase.co';
const String token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVxdnBzYWFyemt0eXR0bmhxem1mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODExODI0MjksImV4cCI6MTk5Njc1ODQyOX0.APiW1oVH-BI2T8dNEj-tZelgiTjLNGx7CbgyVzw7Hps';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Supabase.initialize(url: supabaseUrl, anonKey: token);

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login Mahasiswa",
      debugShowCheckedModeBanner: false,
      initialRoute: 'splash',
      routes: {
        'splash': (_) => const SplashPage(),
        'login': (_) => const LoginPage(),
        '/signup': (_) => const SignUpPage(),
        '/home': (_) => const HomeScreen(),
      },
      theme: ThemeData(primarySwatch: Colors.orange),
      home: const LoginPage(),
    );
  }
}
