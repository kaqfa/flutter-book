import 'package:flutter/material.dart';
import 'package:lapor_book/pages/home_laporanku_page.dart';
import 'package:lapor_book/pages/home_profile_page.dart';
import 'package:lapor_book/supabase_manager.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final supabase = SupabaseManager();
  int _selected_index = 0;
  final List<Widget> _pages = [
    Laporanku(tulisan: "Laporanku"),
    Laporanku(tulisan: "Semua laporan"),
    const ProfilePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selected_index = index;
    });
  }

  void handleClick(String value) {
    switch (value) {
      case 'Logout':
        supabase.logout(context);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Halaman Beranda"),
        actions: <Widget>[
          PopupMenuButton<String>(
              onSelected: handleClick,
              itemBuilder: (BuildContext context) {
                return {
                  'Logout',
                }.map((String choice) {
                  return PopupMenuItem<String>(
                      value: choice, child: Text(choice));
                }).toList();
              })
        ],
      ),
      body: _pages[_selected_index],
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.business_center), label: 'Laporanku'),
          BottomNavigationBarItem(
              icon: Icon(Icons.business), label: 'Semua Laporan'),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_2_rounded), label: 'Profil'),
        ],
        currentIndex: _selected_index,
        selectedItemColor: Colors.red[900],
        onTap: _onItemTapped,
      ),
    );
  }
}
