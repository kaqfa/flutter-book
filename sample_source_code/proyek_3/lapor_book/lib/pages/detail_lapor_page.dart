import 'package:flutter/material.dart';
import 'package:lapor_book/components/laporan.dart';

class DetailLaporanPage extends StatelessWidget {
  final Laporan laporan;

  const DetailLaporanPage({required this.laporan});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Detail Laporan')),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.all(16),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  laporan.foto,
                  height: 200,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    laporan.judul,
                    style: const TextStyle(
                        fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      const Icon(Icons.calendar_today),
                      const SizedBox(
                        width: 4,
                      ),
                      Text(laporan.tanggal),
                      const SizedBox(
                        width: 16,
                      ),
                      const Icon(Icons.person),
                      const SizedBox(
                        width: 4,
                      ),
                      Text(laporan.pelapor)
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    laporan.keterangan,
                    style: const TextStyle(fontSize: 18),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    "Kategori: ${laporan.kategori}",
                    style: const TextStyle(color: Colors.grey),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
