import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lapor_book/components/avatar.dart';
import 'package:lapor_book/supabase_manager.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<StatefulWidget> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {
  final _formKey = GlobalKey<FormState>();
  final _usernameCont = TextEditingController();
  final _fullnameCont = TextEditingController();
  final _websiteCont = TextEditingController();
  String? _avatarUrl;
  final supabase = SupabaseManager();

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 10,
            ),
            Avatar(imageUrl: _avatarUrl, onUpload: _onUpload),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: _usernameCont,
              decoration: const InputDecoration(labelText: "Username"),
            ),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: _fullnameCont,
              decoration: const InputDecoration(labelText: "Nama Lengkap"),
            ),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: _websiteCont,
              decoration: const InputDecoration(labelText: "Website"),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(children: [
              Expanded(
                  child: ElevatedButton(
                      onPressed: _simpanData,
                      child: const Text("Simpan Data"))),
            ])
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _usernameCont.dispose();
    _fullnameCont.dispose();
    _websiteCont.dispose();
    super.dispose();
  }

  Future<void> _getProfile() async {
    try {
      final userId = supabase.client.auth.currentUser!.id;
      final data = await supabase.client
          .from('profiles')
          .select()
          .eq('id', userId)
          .single() as Map;
      _usernameCont.text = (data['username'] ?? '') as String;
      _fullnameCont.text = (data['full_name'] ?? '') as String;
      _websiteCont.text = (data['website'] ?? '') as String;
      _avatarUrl = (data['avatar_url'] ?? '') as String;
    } on PostgrestException catch (error) {
      Fluttertoast.showToast(msg: error.message);
    } catch (error) {
      Fluttertoast.showToast(msg: "Kesalahan lain");
    }
  }

  Future<void> _onUpload(String imageUrl) async {
    try {
      final userId = supabase.client.auth.currentUser!.id;
      await supabase.client
          .from('profiles')
          .upsert({'id': userId, 'avatar_url': imageUrl});
      if (mounted) {
        Fluttertoast.showToast(msg: "Gambar profil terupdate");
      }
    } on PostgrestException catch (error) {
      Fluttertoast.showToast(msg: error.message);
    } catch (error) {
      Fluttertoast.showToast(msg: "Terjadi kesalahan yang tidak terduga");
    }

    if (!mounted) {
      return;
    }

    setState(() {
      _avatarUrl = imageUrl;
    });
  }

  void _simpanData() {}
}
