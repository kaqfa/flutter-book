import 'package:flutter/material.dart';
import 'package:lapor_book/components/laporan.dart';

class LaporankuPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LaporankuPage();
}

class _LaporankuPage extends State<LaporankuPage> {
  List<Laporan> _dataLap = [];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _dataLap.length,
      itemBuilder: (BuildContext context, int index) {
        return ItemLaporan(laporan: _dataLap[index]);
      },
    );
  }
}
