---
title: "Konsep"
date: "2023-05-01"
categories: [flutter]
---

# **Konsep** {.unnumbered}


Learning path :

1. Pengenalan Flutter
2. Dasar Pemrograman Dart
3. Antarmuka Pengguna
4. Routing dan Navigasi
5. Pengujian dan Debugging
6. Akses Data Lokal
7. Akses Data melalui API
8. Integrasi Dengan BAas
9. Layanan Berbasis Lokasi
10. Pengamanan Aplikasi